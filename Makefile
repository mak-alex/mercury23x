OPTIONS = -std=c99

mercury.test: mercury.test.c time.c
	${CC} $^ $(OPTIONS) -o $@ -O0
clean:
	rm mercury.test
